'use strict';

angular
    .module('app', [
        'ngRoute',
        'ngResource',
        'signature'
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainController',
                controllerAs: 'vm'
            })
            .when('/:id', {
                templateUrl: 'views/main.html',
                controller: 'MainController',
                controllerAs: 'vm'
            }) .when('/contract/:id', {
                templateUrl: 'views/contract.html',
                controller: 'ContractController',
                controllerAs: 'vm'
            }).otherwise({
                redirectTo: '/'
            });
    });