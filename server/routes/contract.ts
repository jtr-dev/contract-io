import { Contract as ContractModel } from '../models/Contract'
import { handleResponse} from './../helpers'
/**
 *  Express routes
 */
export class Contract {
	contractModel: ContractModel;
	constructor() {
		this.contractModel = new ContractModel()
	}

	create = (req: any, res: any) => {
		const { body } = req

		this.contractModel.create(body, handleResponse(res))
	}

	get = (req: any, res: any) => {
		const { id } = req.params

		this.contractModel.get(id, handleResponse(res))
	}

	update = (req: any, res: any) => {
		const { body } = req

		this.contractModel.update(body, handleResponse(res))
	}

	delete = (req: any, res: any) => {
		const { id } = req.params

		this.contractModel.delete(id, handleResponse(res))
	}

}
