import { Document } from "mongoose";

export default interface IContract extends Document {
    id: Number,
    title: String,
    day: Number,
    month: Number,
    year: Number,
    sections?: [
        {
            id: Number,
            title: String,
            overview: String,
            description: String
        }
    ],
    contractor: {
        name: String,
        sig: Object,
        date: Date
    },
    owner: {
        name: String,
        sig: Object,
        date: Date
    },
    state: String
}
